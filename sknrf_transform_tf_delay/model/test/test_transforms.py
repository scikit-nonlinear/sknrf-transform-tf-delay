import os
import unittest
import pickle

import torch as th
from  numpy.testing import *

from sknrf.enums.device import Instrument
from sknrf.enums.runtime import SI, si_dtype_map
from sknrf.settings import Settings
from sknrf.model.base import AbstractModel

from sknrf_transform_tf_delay.model.delay import InstrumentDelayTransform, SampleDelayTransform, PhaseNormalizationTransform


__author__ = 'dtbespal'


class TestTransform(unittest.TestCase):
    class_ = InstrumentDelayTransform
    args = ("Delay", (0,), Instrument.ALL, None, th.full((Settings().f_points,), -2*Settings().t_step, dtype=si_dtype_map[SI.T]))
    filename = os.sep.join([Settings().data_root, "saved_transforms", "Thru.s2p"])

    @classmethod
    def setUpClass(cls):
        AbstractModel.init_test(cls.__name__)
        cls.p_filename = os.sep.join([Settings().data_root, "testdata", "saved_state.p"])

    def setUp(self):
        self.transform = self.class_(*self.args)

    def test_default_args(self):
        self.transform = self.class_()
        self.test_get_stimulus()
        self.test_set_stimulus()
        self.test_get_response()

    def test_get_stimulus(self):
        shape = (1, Settings().t_points, Settings().f_points)
        args = []
        class_ = th.Tensor
        for port_index in range(Settings().num_ports + 1):
            port_args = []
            port_args.append(th.rand(shape, dtype=si_dtype_map[SI.V]))
            port_args.append(th.rand(shape, dtype=si_dtype_map[SI.I]))
            port_args.append(th.rand(shape, dtype=si_dtype_map[SI.Z]))
            args.append(port_args)
        res_args = self.transform.get_stimulus(*args)
        for port_index in range(Settings().num_ports + 1):
            self.assertIsInstance(res_args[port_index][0], class_)
            self.assertIsInstance(res_args[port_index][1], class_)
            self.assertIsInstance(res_args[port_index][2], class_)

    def test_set_stimulus(self):
        shape = (1, Settings().t_points, Settings().f_points)
        args = []
        class_ = th.Tensor
        for port_index in range(Settings().num_ports + 1):
            port_args = []
            port_args.append(th.rand(shape, dtype=si_dtype_map[SI.V]))
            port_args.append(th.rand(shape, dtype=si_dtype_map[SI.I]))
            port_args.append(th.rand(shape, dtype=si_dtype_map[SI.Z]))
            args.append(port_args)
        res_args = self.transform.set_stimulus(*args)
        for port_index in range(Settings().num_ports + 1):
            self.assertIsInstance(res_args[port_index][0], class_)
            self.assertIsInstance(res_args[port_index][1], class_)
            self.assertIsInstance(res_args[port_index][2], class_)

    def test_get_response(self):
        shape = (1, Settings().t_points, Settings().f_points)
        args = []
        class_ = th.Tensor
        for port_index in range(Settings().num_ports + 1):
            port_args = []
            port_args.append(th.rand(shape, dtype=si_dtype_map[SI.V]))
            port_args.append(th.rand(shape, dtype=si_dtype_map[SI.I]))
            port_args.append(th.rand(shape, dtype=si_dtype_map[SI.Z]))
            args.append(port_args)
        res_args = self.transform.get_response(*args)
        for port_index in range(Settings().num_ports + 1):
            self.assertIsInstance(res_args[port_index][0], class_)
            self.assertIsInstance(res_args[port_index][1], class_)
            self.assertIsInstance(res_args[port_index][2], class_)

    def test_save(self):
        if os.path.exists(self.p_filename):
            os.remove(self.p_filename)
        pickle.dump(self.transform, open(self.p_filename, "wb"))
        self.assertTrue(os.path.exists(self.p_filename))

    def test_save_load(self):
        self.test_save()
        del self.transform

        self.transform = pickle.load(open(self.p_filename, "rb"))
        self.assertIsInstance(self.transform, self.class_)

    def test_file(self):
        if len(self.filename):
            file = open(self.filename, 'r')
            file.close()
            self.transform.file = file
            self.assertTrue(self.transform.file.closed)

    def test_color(self):
        self.assertIsInstance(self.transform.color(), str)

    def test_release(self):
        self.transform.release()

    def test_ports(self):
        ports = tuple(range(self.transform._num_ports+1))
        self.assertRaisesRegex(ValueError,
                               "%s is a %d-Port Transform" % (self.transform.__class__.__name__, self.transform._num_ports,),
                               self.transform.__class__.ports.fset,
                               self.transform, ports)
        ports = list(range(self.transform._num_ports))
        ports[0] = Settings().num_ports + 1
        ports = tuple(ports)
        self.assertRaisesRegex(ValueError,
                               "Ports entry exceeds the maximum number of ports: %d " % Settings().num_ports,
                               self.transform.__class__.ports.fset,
                               self.transform, ports)
        ports = tuple(range(self.transform._num_ports))
        self.transform.ports = ports

    def tearDown(self):
        pass


class TestInstrumentDelayTransform(TestTransform):
    class_ = InstrumentDelayTransform
    args = ("Delay", (0,), Instrument.ALL, None, th.full((Settings().f_points,), -2*Settings().t_step, dtype=si_dtype_map[SI.T]))
    filename = ""

    def test_delay(self):
        self.transform.delay = th.zeros((Settings().f_points,), dtype=si_dtype_map[SI.T])
        eps = th.finfo(si_dtype_map[SI.T]).eps
        assert_allclose(self.transform.delay, 0.0, rtol=eps, atol=eps)

    def test_constant(self):
        self.transform.constant = th.zeros((1,), dtype=si_dtype_map[SI.T])
        eps = th.finfo(si_dtype_map[SI.T]).eps
        assert_allclose(self.transform.constant, 0.0, rtol=eps, atol=eps)
        assert_allclose(self.transform.delay, 0.0, rtol=eps, atol=eps)


class TestSampleDelayTransform(TestTransform):
    class_ = SampleDelayTransform
    args = ("Sample Delay", (0,), Instrument.ALL, None, th.full((Settings().f_points,), -2*Settings().t_step, dtype=si_dtype_map[SI.T]), True)
    filename = ""

    def test_delay(self):
        pass

    def test_constant(self):
        pass


class TestPhaseNormalizationTransform(TestTransform):
    class_ = PhaseNormalizationTransform
    args = ("Phase-Norm", (), Instrument.ALL, None, 1, 0)
    filename = ""

