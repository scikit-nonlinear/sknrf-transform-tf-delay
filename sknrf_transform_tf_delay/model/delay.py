import torch as th

from sknrf.enums.device import Instrument
from sknrf.enums.signal import transform_color_map
from sknrf.settings import Settings
from sknrf.device.signal import tf
from sknrf.model.transform.tf.base import TFTransform
from sknrf.utilities.numeric import Info, Scale, PkAvg, Format, Domain, bounded_property


class InstrumentDelayTransform(TFTransform):
    """Delay Transform

    ..  figure:: ../../_images/PNG/phase_normalization_circuit_transform.png
        :width: 50 %
        :align: center

    Two correlated signals can be aligned by determining the cross-correlation maxima (minimia) for positively
    (negatively) correlated signals. The following equation assumes that we take the maxima.

    .. math::
        \tau_\mathrm{delay}=argmax((f \star g)(t))

    where:
        * $f$ is the reference signal.
        * $g$ is the target signal to be delayed to align with $f$

    The cross-correlation is a convolution of $f$ and $g$ in the time-domain, which can be converted to a multiplication in the frequency-domain.

    .. math::
        \tau_\mathrm{delay}=argmax(\mathcal{F}\{f\} \cdot \mathcal{F}\{g\}^*)

    Notes
    -----
    * Since delay calculated here is always positive:
        * **The target signal $g$ must always *lag* the reference signal $f$ by < $t_{stop}$.**
    * Signals with sub-periodic periodic behaviour have additional requirements:
        * **The target signal $g$ must always *lag* the reference signal $f$ by < T (where T < $t_{stop}$).**
        * **You must specify the period, by default T = $t_{stop}$.**

    Parameters
    ----------
    name : str
        Name of the transform instance.
    ports : tuple
        Port numbers that apply the transform.

    """
    _num_ports = 1
    _domain = Domain.TF
    _device = "cpu"
    _preview_filename = ":/PNG/t_circuit_transform.png"
    _default_filename = ""
    _color_code = Settings().color_map[transform_color_map[Domain.TF]]
    display_order = ["name", "ports", "instrument_flags", "constant", "delay"]
    optimize = False

    def __new__(cls, name: str = "Delay", ports: tuple = (1,), instrument_flags=Instrument.ALL,
                data: th.Tensor = None, delays: th.Tensor = th.zeros((1,))):
        self = super(InstrumentDelayTransform, cls).__new__(cls, name, ports, instrument_flags=instrument_flags,
                                                            data=data)
        self._constant = 0.0
        self._delay = delays + th.zeros(Settings().f_points, dtype=delays.dtype)
        return self

    def __getnewargs__(self):
        state = super(InstrumentDelayTransform, self).__getnewargs__()
        state = tuple(list(state) +
                      [self._delay])
        return state

    def __init__(self, name: str = "Delay", ports: tuple = (1,), instrument_flags=Instrument.ALL,
                 data: th.Tensor = None, delays: th.Tensor = th.zeros((1,))):
        super(InstrumentDelayTransform, self).__init__(name, ports, instrument_flags=instrument_flags,
                                                       data=data)

        self.__info__()

        # Initialize object PROPERTIES
        self.set_delay(self._delay)

    def release(self):
        self.set_delay(-self._delay)

    def __getstate__(self, state={}):
        super(InstrumentDelayTransform, self).__getstate__(state)
        state["_constant"] = self._constant
        return state

    def __setstate__(self, state):
        super(InstrumentDelayTransform, self).__setstate__(state)

        self.__info__()

        # Initialize object PROPERTIES
        self._constant = state["_constant"]
        self.set_delay(self._delay)

    def __info__(self):
        super(InstrumentDelayTransform, self).__info__()
        # ### Manually generate info of ATTRIBUTES and PROPERTIES here ###
        self.info["file"] = Info("file", read=False, write=False, check=False)
        self.info["delays"] = Info("delays", read=False, write=False, check=False, min_=-1, max_=1)
        self.info["constant"] = Info("constant", read=True, write=True, check=False,
                                     format_=Format.RE, scale=Scale.u, unit="s",
                                     min_=0.0, abs_tol=1e-10, rel_tol=1e-10)

    @bounded_property
    def delay(self):
        return self._delay

    @delay.setter
    def delay(self, delay):
        self.set_delay(-self._delay)
        self.set_delay(delay)
        self._delay = delay

    @bounded_property
    def constant(self):
        return self._constant

    @constant.setter
    def constant(self, constant):
        self.delay = constant + th.zeros((Settings().f_points,), dtype=constant.dtype)
        self._constant = constant

    def set_delay(self, delays):
        devices = self.device_model()
        port_index = self.ports[0]
        if self.instrument_flags & Instrument.LFSOURCE:
            devices.ports[port_index].lfsource._trigger_delay += delays[0]
        if self.instrument_flags & Instrument.LFRECEIVER:
            devices.ports[port_index].lfreceiver._trigger_delay += delays[0]
        if self.instrument_flags & Instrument.LFZTUNER:
            devices.ports[port_index].lfztuner._trigger_delay += delays[0]
        if self.instrument_flags & Instrument.RFSOURCE:
            devices.ports[port_index].rfsource._trigger_delay += delays[1]
        if self.instrument_flags & Instrument.RFRECEIVER:
            devices.ports[port_index].rfreceiver._trigger_delay += delays[1]
        if self.instrument_flags & Instrument.RFZTUNER:
            devices.ports[port_index].rfztuner._trigger_delay += delays[1]

    def get_stimulus(self, *args):
        return args

    def set_stimulus(self, *args):
        return args

    def get_response(self, *args):
        return args


class SampleDelayTransform(TFTransform):
    """Delay Transform

    ..  figure:: ../../_images/PNG/phase_normalization_circuit_transform.png
        :width: 50 %
        :align: center

    Two correlated signals can be aligned by determining the cross-correlation maxima (minimia) for positively
    (negatively) correlated signals. The following equation assumes that we take the maxima.

    .. math::
        \tau_\mathrm{delay}=argmax((f \star g)(t))

    where:
        * $f$ is the reference signal.
        * $g$ is the target signal to be delayed to align with $f$

    The cross-correlation is a convolution of $f$ and $g$ in the time-domain, which can be converted to a multiplication in the frequency-domain.

    .. math::
        \tau_\mathrm{delay}=argmax(\mathcal{F}\{f\} \cdot \mathcal{F}\{g\}^*)

    Notes
    -----
    * Since delay calculated here is always positive:
        * **The target signal $g$ must always *lag* the reference signal $f$ by < $t_{stop}$.**
    * Signals with sub-periodic periodic behaviour have additional requirements:
        * **The target signal $g$ must always *lag* the reference signal $f$ by < T (where T < $t_{stop}$).**
        * **You must specify the period, by default T = $t_{stop}$.**

    Parameters
    ----------
    name : str
        Name of the transform instance.
    ports : tuple
        Port numbers that apply the transform.

    """

    _num_ports = 1
    _domain = Domain.TF
    _device = "cpu"
    _preview_filename = ":/PNG/t_circuit_transform.png"
    _default_filename = ""
    _color_code = Settings().color_map[transform_color_map[Domain.TF]]
    display_order = ["name", "ports", "instrument_flags", "constant", "zero_pad"]
    optimize = False

    def __new__(cls, name: str = "Sample Delay", ports: tuple = (), instrument_flags=Instrument.ALL,
                data: th.Tensor = None, delays: th.Tensor = th.zeros((1,)), zero_pad=True):
        ports = ports if len(ports) else [1]
        self = super(SampleDelayTransform, cls).__new__(cls, name, ports, instrument_flags=instrument_flags,
                                                        data=data)
        self.zero_pad = zero_pad
        self._constant = 0.0
        self._delay = delays + th.zeros(Settings().f_points, dtype=delays.dtype)
        self._shift = th.round(-self._delay/Settings().t_step).type(th.int)
        return self

    def __getnewargs__(self):
        state = super(SampleDelayTransform, self).__getnewargs__()
        state = tuple(list(state) +
                      [self._delay, self.zero_pad])
        return state

    def __init__(self, name: str = "Sample Delay", ports: tuple = (), instrument_flags=Instrument.ALL,
                 data: th.Tensor = None, delays: th.Tensor = th.zeros((1,)), zero_pad=True):
        ports = ports if len(ports) else [1]
        super(SampleDelayTransform, self).__init__(name, ports, instrument_flags=instrument_flags,
                                                   data=data)

        self.__info__()

        # Initialize object PROPERTIES
        self.zero_pad = zero_pad
        self._delay = delays + th.zeros((Settings().f_points,), dtype=delays.dtype)
        self._shift = th.round(-self._delay/Settings().t_step).type(th.int)

    def __getstate__(self, state={}):
        super(SampleDelayTransform, self).__getstate__(state)
        state["_constant"] = self._constant
        return state

    def __setstate__(self, state):
        super(SampleDelayTransform, self).__setstate__(state)

        self.__info__()

        # Initialize object PROPERTIES
        self._constant = state["_constant"]

    def __info__(self):
        super(SampleDelayTransform, self).__info__()
        # ### Manually generate info of ATTRIBUTES and PROPERTIES here ###
        self.info["file"] = Info("file", read=False, write=False, check=False)
        self.info["constant"] = Info("constant", read=True, write=True, check=False,
                                     format_=Format.RE, scale=Scale.u, unit="s",
                                     min_=0.0, abs_tol=1e-10, rel_tol=1e-10)
        self.info["zero_pad"] = Info("zero_pad", read=True, write=True, check=False)

    @bounded_property
    def constant(self):
        return self._constant

    @constant.setter
    def constant(self, constant):
        self._delay = constant + th.zeros((Settings().f_points,), dtype=constant.dtype)
        self._shift = th.round(-self._delay/Settings().t_step).type(th.int)
        self._constant = constant

    def get_stimulus(self, *args):
        return args

    def set_stimulus(self, *args):
        p = self.ports[0]
        v, i, z = args[p][0], args[p][1], args[p][2]
        s = v.shape

        shift = int(self._shift[0])
        if self.instrument_flags & Instrument.STIMULUS:
            vi = th.stack((v, i), dim=-1)
            vi = th.roll(vi, shift, dims=-3)
            if self.zero_pad:
                if shift < 0:
                    vi[..., shift:, :, :] = 0.0
                elif shift > 0:
                    vi[..., :shift, :, :] = 0.0
            v, i = th.split(vi, 1, dim=-1)

        res = list(args)
        res[p][0], res[p][1], res[p][2] = v.reshape(s), i.reshape(s), z.reshape(s)
        return tuple(res)

    def get_response(self, *args):
        p = self.ports[0]
        v, i, z = args[p][0], args[p][1], args[p][2]
        s = v.shape

        shift = int(self._shift[0])
        if self.instrument_flags & Instrument.RESPONSE:
            vi = th.stack((v, i), dim=-1)
            vi = th.roll(vi, shift, dims=-3)
            if self.zero_pad:
                if shift < 0:
                    vi[..., shift:, :, :] = 0.0
                elif shift > 0:
                    vi[..., :shift, :, :] = 0.0
            v, i = th.split(vi, 1, dim=-1)

        res = list(args)
        res[p][0], res[p][1], res[p][2] = v.reshape(s), i.reshape(s), z.reshape(s)
        return tuple(res)


class PhaseNormalizationTransform(TFTransform):
    r"""Phase Normalization Transform

    ..  figure:: ../../_images/PNG/phase_normalization_circuit_transform.png
        :width: 50 %
        :align: center

    .. math::
        \left[\begin{array}{c} b_{1} \\ a_{1} \\ b_{2} \\ a_{2} \end{array} \right] =
        \left[\begin{array}{cccc}
            P^{-l} & 0      & 0      & 0      \\
            0      & P^{-l} & 0      & 0      \\
            0      & 0      & P^{-l} & 0      \\
            0      & 0      & 0      & P^{-l} \\
        \end{array} \right]
        \left[\begin{array}{c} b_{1}' \\ a_{1}' \\ b_{2}' \\ a_{2}' \end{array} \right]

    A phase-normalization circuit transform on port 1 and 2 of a 2-port system, where math::P^{-1} is the phase of
    the incident-wave of the selected port. This transform is automatically applied to all system ports.

    Parameters
    ----------
    name : str
        Name of the transform instance.

    """

    _num_ports = Settings().num_ports
    _domain = Domain.TF
    _device = "cpu"
    _preview_filename = ":/PNG/phase_normalization_circuit_transform.png"
    _default_filename = ""
    _color_code = Settings().color_map[transform_color_map[Domain.TF]]
    display_order = ["name", "ports", "instrument_flags", "ref_port", "ref_signal"]
    optimize = False

    def __new__(cls, name: str = "Phase-Norm", ports: tuple = (), instrument_flags=Instrument.ALL,
                data: th.Tensor = None,
                ref_port=1, ref_signal=0):
        ports = ports if len(ports) else list(range(1, Settings().num_ports+1))
        self = super(PhaseNormalizationTransform, cls).__new__(cls, name, ports, instrument_flags=instrument_flags,
                                                               data=data)
        self.ref_port = ref_port
        self.ref_signal = ref_signal
        self._p_inv = th.ones((Settings().t_points, Settings().f_points, len(self.ports), 2))
        return self

    def __getnewargs__(self):
        state = super(PhaseNormalizationTransform, self).__getnewargs__()
        state = tuple(list(state) +
                      [self.ref_port, self.ref_signal])
        return state

    def __init__(self, name: str = "Phase-Norm", ports: tuple = (), instrument_flags=Instrument.ALL,
                 data: th.Tensor = None,
                 ref_port=1, ref_signal=1):
        ports = ports if len(ports) else list(range(1, Settings().num_ports + 1))
        super(PhaseNormalizationTransform, self).__init__(name, ports, instrument_flags=instrument_flags,
                                                          data=data)
        self.__info__()
        # Initialize object PROPERTIES

    def __info__(self):
        super(PhaseNormalizationTransform, self).__info__()
        # ### Manually generate info of ATTRIBUTES and PROPERTIES here ###
        self.info["ref_port"] = Info("ref port", read=True, write=True, check=True)
        self.info["ref_signal"] = Info("ref signal", read=True, write=True, check=True)
        self.info["file"] = Info("file", read=False, write=False, check=False)

    def get_stimulus(self, *args):
        ref_pk = tf.pk(args[self.ref_port][self.ref_signal][..., 1:2])

        h = th.as_tensor(Settings().harmonics, dtype=ref_pk.dtype)
        self._p_inv = (ref_pk.abs()/ref_pk)**h

        res = list(args)
        for arg_index in range(len(args)):
            res[arg_index][0] = res[arg_index][0]*self._p_inv
            res[arg_index][1] = res[arg_index][1]*self._p_inv
        return tuple(res)

    def set_stimulus(self, *args):
        ref_pk = tf.pk(args[self.ref_port][self.ref_signal][..., 1:2])

        h = th.as_tensor(Settings().harmonics, dtype=ref_pk.dtype)
        self._p_inv = (ref_pk.abs()/ref_pk)**h

        res = list(args)
        for arg_index in range(len(args)):
            res[arg_index][0] = res[arg_index][0]/self._p_inv
            res[arg_index][1] = res[arg_index][1]/self._p_inv
        return tuple(res)

    def get_response(self, *args):
        ref_pk = tf.pk(args[self.ref_port][self.ref_signal][..., 1:2])

        h = th.as_tensor(Settings().harmonics, dtype=ref_pk.dtype)
        self._p_inv = (ref_pk.abs()/ref_pk)**h

        res = list(args)
        for arg_index in range(len(args)):
            res[arg_index][0] = res[arg_index][0]*self._p_inv
            res[arg_index][1] = res[arg_index][1]*self._p_inv
        return tuple(res)
